import React from "react";

import validator from "validator";

import "./Signup.css";

const styles = {
  iconStyle: {
    margin: "1.7rem 0 0 1rem",
    color: "#0b5203",
  },
};

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      role: "",
      confirmPassword: "",
      isSubmit: false,
      validation: false,
    };
  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    this.setState({ isSubmit: true });
    let { firstname, lastname, email, role, password, confirmPassword } =
      this.state;
    if (
      validator.isAlpha(firstname) &&
      validator.isAlpha(lastname) &&
      validator.isEmail(email) &&
      validator.isStrongPassword(password) &&
      validator.equals(confirmPassword, password)
    ) {
      console.log("validation done");
      this.setState({
        validation: true,
        firstname: "",
        lastname: "",
        email: "",
        role: "",
        password: "",
        confirmPassword: "",
        isSubmit: false,
      });
      event.target.reset();
    } else {
      this.setState({ validation: false });
    }
  };

  handleFirstName = (event) => {
    this.setState({ firstname: event.target.value });
  };

  handleLastName = (event) => {
    this.setState({ lastname: event.target.value });
  };

  handleEmail = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  handleConfirmPassword = (event) => {
    this.setState({ confirmPassword: event.target.value });
  };

  handleRole = (event) => {
    this.setState({ role: event.target.value });
  };

  render() {
    const {
      firstname,
      lastname,
      email,
      password,
      confirmPassword,
      isSubmit,
      validation,
    } = this.state;
    return (
      <div className="form-container">
        <form
          onSubmit={this.handleFormSubmit}
          className={validation ? "form form-opacity" : "form"}
        >
          <h1 className="text-center mb-4">Create New Account</h1>
          <div className="form-group">
            <label htmlFor="first-name">First Name</label>
            <div className="input-container">
              <i
                className="fa-solid fa-user fa-xl"
                style={styles.iconStyle}
              ></i>
              <input
                type="text"
                id="first-name"
                className="form-control"
                placeholder="Enter Firstname"
                maxLength="20"
                value={firstname}
                onChange={this.handleFirstName}
                required
              />
            </div>
            <p className="error-msg">
              {!validator.isAlpha(firstname) && firstname !== ""
                ? "Please enter correct input"
                : ""}
            </p>
          </div>
          <div className="form-group">
            <label htmlFor="last-name">Last Name</label>
            <div className="input-container">
              <i
                className="fa-solid fa-user fa-xl"
                style={styles.iconStyle}
              ></i>
              <input
                type="text"
                id="last-name"
                className="form-control"
                placeholder="Enter Lastname"
                maxLength="20"
                value={lastname}
                onChange={this.handleLastName}
                required
              />
            </div>
            <p className="error-msg">
              {!validator.isAlpha(lastname) && lastname !== ""
                ? "Please enter correct input"
                : ""}
            </p>
          </div>
          <div className="form-group">
            <label htmlFor="age">Age</label>
            <div className="input-container">
              <input
                type="number"
                id="age"
                className="form-control"
                placeholder="Enter Age"
                max="120"
                required
              />
            </div>
          </div>
          <div>
            <label>Gender</label>
            <div className="d-flex flex-row radio-gender">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="male"
                  value="male"
                  required
                />
                <label className="form-check-label" htmlFor="male">
                  Male
                </label>
              </div>

              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="female"
                  value="female"
                />
                <label className="form-check-label" htmlFor="female">
                  Female
                </label>
              </div>

              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="female"
                  value="female"
                />
                <label className="form-check-label" htmlFor="female">
                  Others
                </label>
              </div>
            </div>
          </div>
          <label htmlFor="role">Role</label>
          <select
            className="form-select form-control mb-4"
            id="role"
            required
            onChange={this.handleRole}
          >
            <option value="">Select Role</option>
            <option value="Developer">Developer</option>
            <option value="seniorDeveloper">Senior Developer</option>
            <option value="leadEngineer">Lead Engineer</option>
            <option value="cto">CTO</option>
          </select>

          <div className="form-group">
            <label htmlFor="email">Email</label>
            <div className="input-container">
              <i
                className="fa-solid fa-envelope fa-xl"
                style={styles.iconStyle}
              ></i>
              <input
                type="email"
                id="email"
                placeholder="Enter Email"
                className="form-control"
                value={email}
                required
                onChange={this.handleEmail}
              />
            </div>
            <p className="error-msg">
              {!validator.isEmail(email) && email !== "" && isSubmit
                ? "Please enter correct email"
                : ""}
            </p>
          </div>

          <div className="form-group">
            <label htmlFor="password">Password</label>
            <div className="input-container">
              <i
                className="fa-solid fa-lock fa-xl"
                style={styles.iconStyle}
              ></i>
              <input
                type="password"
                id="password"
                placeholder="Enter Password atleast 8 char"
                className="form-control"
                onChange={this.handlePassword}
                value={password}
                maxLength="25"
                required
              />
            </div>
            <p className="error-msg">
              {isSubmit &&
              !validator.isStrongPassword(password, {
                minLength: 8,
                minLowercase: 1,
                minUppercase: 1,
                minNumbers: 1,
                minSymbols: 1,
              })
                ? "Please enter valid password"
                : ""}
            </p>
          </div>

          <div className="form-group">
            <label htmlFor="confirm-password">Confirm Password</label>
            <div className="input-container">
              <i
                className="fa-solid fa-lock fa-xl"
                style={styles.iconStyle}
              ></i>
              <input
                type="password"
                id="confirm-password"
                placeholder="Confirm Password"
                className="form-control"
                value={confirmPassword}
                onChange={this.handleConfirmPassword}
                required
              />
            </div>
            <p className="error-msg">
              {isSubmit && !validator.equals(confirmPassword, password)
                ? "Please enter same as password"
                : ""}
            </p>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value=""
              id="checkbox"
              required
            />
            <label className="form-check-label" htmlFor="checkbox">
              I Agree to Terms and Conditions
            </label>
          </div>
          <button type="submit" className="btn btn-success">
            Submit
          </button>
        </form>
        {validation && (
          <div className="d-flex flex-column align-items-center pt-5 pb-5 success-container">
            <h1>Thank You!!</h1>
            <p>Signup completed Successfully</p>
          </div>
        )}
      </div>
    );
  }
}

export default Signup;
