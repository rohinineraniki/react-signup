import "./App.css";
import Signup from "./components/Signup";

function App() {
  return (
    <div className="bg-container">
      <Signup />
    </div>
  );
}

export default App;
